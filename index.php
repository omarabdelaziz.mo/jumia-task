<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

use App\Controller\HomeController;
use App\Route\Router;

$router = new Router();

$router->get('/', function (){
    return (new HomeController())->execute();
});
$router->addNotFoundHandler(function(){
    include('views/404.html');
});
$router->run();
