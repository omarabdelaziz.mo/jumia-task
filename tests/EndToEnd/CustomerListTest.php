<?php



namespace tests\src\Model;

use App\DB\Builder\Builder;
use App\DB\Connection;
use App\DB\QueryBuilder;
use App\Model\Customer;
use App\Repository\CustomerRepository;
use PHPUnit\Framework\TestCase;


class CustomerListTest extends TestCase
{
    private $url = 'http://localhost:8000/';

    public function testCustomerList()
    {
        $data = file_get_contents("$this->url");
        $this->assertEquals(42, substr_count($data, '<tr>'));
    }


    public function testCustomerListWithValidPhoneNumberOnly()
    {
        $data = file_get_contents("$this->url?validPhone=OK");
        // tr of the head and 27 for the customers
        $this->assertEquals(28, substr_count($data, '<tr>'));
    }

    public function testFilterCustomerByCountry()
    {
        $data = file_get_contents("$this->url?country=%2B212");
        // tr of the  head + 7 of the customers
        $this->assertEquals(8, substr_count($data, '<tr>'));
    }
    public function testFilterCustomerByCountryAndValidPhone()
    {
        $data = file_get_contents("$this->url?country=%2B212&validPhone=OK");
        // tr of the  head + 4 of the customers
        $this->assertEquals(5, substr_count($data, '<tr>'));
    }

}