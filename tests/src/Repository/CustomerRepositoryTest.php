<?php

declare(strict_types=1);

namespace tests\src\Model;

use App\DB\Builder\Builder;
use App\DB\Connection;
use App\DB\QueryBuilder;
use App\Model\Customer;
use App\Repository\CustomerRepository;
use PHPUnit\Framework\TestCase;


class CustomerRepositoryTest extends TestCase
{
    public function testFetchAllBy()
    {
        
        $connection = new Connection();
        $builder = new Builder();
        $queryBuilder = new QueryBuilder($builder);
        $customerRepository = new CustomerRepository($connection, $queryBuilder);
        $customers = $customerRepository->findAllBy('customer', ['name', '=', 'Younes Boutikyad']);
        $this->assertEquals(1, count($customers));
        $this->assertEquals('Younes Boutikyad', $customers[0]->getName());
    }

    public function testRetrieveCustomerByCountryCode()
    {
        $connection = new Connection();
        $builder = new Builder();
        $queryBuilder = new QueryBuilder($builder);
        $customerRepository = new CustomerRepository($connection, $queryBuilder);
        $customers = $customerRepository->findAllBy('customer', ['phone', 'like', '%(212%']);
        $this->assertEquals(7, count($customers));
    }
}