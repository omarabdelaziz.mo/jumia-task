<?php

declare(strict_types=1);

namespace tests\src\Model;

use App\Model\Customer;
use PHPUnit\Framework\TestCase;


class CustomerTest extends TestCase
{
    public function testCustomerHydrate()
    {
        $customer = new Customer();
        $customer->setId(1)->setName('John')->setPhone('+380(99)999-99-99');
        $this->assertEquals(1, $customer->getId());
        $this->assertEquals('John', $customer->getName());
        $this->assertEquals('+380(99)999-99-99', $customer->getPhone());
    }

    public function testCustomerValidPhoneNumber()
    {
        $customer = new Customer();
        $customer->setId(1)->setName('Yosaf Karrouch')->setPhone('(212) 698054317');
        $this->assertEquals('OK', $customer->isPhoneValid());
    }

    public function testCustomerInValidPhoneNumber()
    {
        $customer = new Customer();
        $customer->setId(1)->setName('Yosaf Karrouch')->setPhone('(212) 6007989253');
        $this->assertEquals('NOK', $customer->isPhoneValid());
    }

    public function testCustomerCountryCode()
    {
        $customer = new Customer();
        $customer->setId(1)->setName('Walid Hammadi')->setPhone('(212) 6007989253');
        $this->assertEquals('212', $customer->getPhoneCountryCode());
    }

    public function testCustomerCountry()
    {
        $customer = new Customer();
        $customer->setId(1)->setName('Walid Hammadi')->setPhone('(212) 6007989253');
        $this->assertEquals('Morocco', $customer->getCountry());
    }
}