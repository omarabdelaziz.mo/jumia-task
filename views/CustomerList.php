<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Customer List</title>
</head>
<body>
    <header>
        <h1>Phone Number</h1>
    </header>
    <section style="display: flex; justify-content:flex-start; margin-bottom: 100px">
        <div id="countryPicker" style="margin-right: 30px">
            <label for="country">Select Country</label>
            <select name="country" id="country">
                <option value="">--- Choose a color ---</option>
                <?php foreach ($countries as $key => $value): ?>
                    <option value="<?= $key?>" ><?= $value?></option>
                
               
            <?php endforeach; ?>
             
            </select>
        </div>

        <div id="validPhoneNumberPicker">
            <label for="validPhoneNumberPicker">Choose Valid/Invalid Phone number</label>
            <select name="validPhone" id="validPhone">
                <option value="">--- Select Valid/Invalid ---</option>
                <option value="OK">Valid</option>
                <option value="NOK">InValid</option>
            </select>
        </div>
        
    </section>
    <table width="100%" cellspacing="0" cellpadding="0" style="border: 1px solid black;">
        <thead>
            <tr>
                <th style="border: 1px solid black;">Country</th>
                <th style="border: 1px solid black;">Name</th>
                <th style="border: 1px solid black;">State</th>
                <th style="border: 1px solid black;">Country Code</th>
                <th style="border: 1px solid black;">Phone Number</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customers as $customer): ?>
                <tr>
                    <td style="text-align: center;border: 1px solid black;"><?= $customer->getCountry() ?></td>
                    <td style="text-align: center;border: 1px solid black;"><?= $customer->getName() ?></td>
                    <td style="text-align: center;border: 1px solid black;"><?= $customer->isPhoneValid() ?></td>
                    <td style="text-align: center;border: 1px solid black;"><?= $customer->getPhoneCountryCode() ?></td>
                    <td style="text-align: center;border: 1px solid black;"><?= $customer->getNumber() ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <script type="text/javascript">

            const countrySelect = document.getElementById('country');

            countrySelect.addEventListener('change', function handleChange(event) {
                insertParam('country',countrySelect.options[countrySelect.selectedIndex].value);
            });
            const phoneSelect = document.getElementById('validPhone');

            phoneSelect.addEventListener('change', function handleChange(event) {
                insertParam('validPhone',phoneSelect.options[phoneSelect.selectedIndex].value);
            });

            function insertParam(key, value) {
                key = encodeURIComponent(key);
                value = encodeURIComponent(value);

                // kvp looks like ['key1=value1', 'key2=value2', ...]
                var kvp = document.location.search.substr(1).split('&');
                let i=0;

                for(; i<kvp.length; i++){
                    if (kvp[i].startsWith(key + '=')) {
                        let pair = kvp[i].split('=');
                        pair[1] = value;
                        kvp[i] = pair.join('=');
                        break;
                    }
                }

                if(i >= kvp.length){
                    kvp[kvp.length] = [key,value].join('=');
                }

                // can return this or...
                let params = kvp.join('&');

                // reload page with new params
                document.location.search = params;
            }

            


            function insertParam(key, value) {
                key = encodeURIComponent(key);
                value = encodeURIComponent(value);

                // kvp looks like ['key1=value1', 'key2=value2', ...]
                var kvp = document.location.search.substr(1).split('&');
                let i=0;

                for(; i<kvp.length; i++){
                    if (kvp[i].startsWith(key + '=')) {
                        let pair = kvp[i].split('=');
                        pair[1] = value;
                        kvp[i] = pair.join('=');
                        break;
                    }
                }

                if(i >= kvp.length){
                    kvp[kvp.length] = [key,value].join('=');
                }

                // can return this or...
                let params = kvp.join('&');

                // reload page with new params
                document.location.search = params;
            }
        </script>
</body>
</html>