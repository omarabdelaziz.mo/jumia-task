<?php

namespace Config;

class Config
{
    /**
     * Path to sqlite database file
     */
    const SQLITE_DB_PATH =  __DIR__.'/../../db/sample.db';

    const COUNTRY_CODE_REGEX = [
        '+237' => '{\(237\)\ ?[2368]\d{7,8}$}',
        '+251' => '{\(251\)\ ?[1-59]\d{8}$}',
        '+212' => '{\(212\)\ ?[5-9]\d{8}$}', 
        '+258' => '{\(258\)\ ?[28]\d{7,8}$}',
        '+256' => '{\(256\)\ ?\d{9}$}'
    ];
    const COUNTRIES = [
        '+237' => 'Cameroon',
        '+251' => 'Ethiopia',
        '+212' => 'Morocco', 
        '+258' => 'Mozambique',
        '+256' => 'Uganda'
    ];

    const COUNTRY_CODE_CODE = [
        '+237' => '(237)',
        '+251' => '(251)',
        '+212' => '(212)', 
        '+258' => '(258)',
        '+256' => '(256)'
    ];
}
