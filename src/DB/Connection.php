<?php

namespace App\DB;

use Config\Config;
use Exception;
use PDO;

class Connection
{

    private static $connection;

    public function open(): PDO
    {
        if (!self::$connection instanceof PDO) {

            try {
                self::$connection = new PDO('sqlite:' . Config::SQLITE_DB_PATH);

            } catch (Exception $e) {
                echo "Unable to connect";
                echo $e;
                exit;
            }

        }

        return self::$connection;
    }

}
