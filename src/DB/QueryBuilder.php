<?php

namespace App\DB;

use App\DB\Builder\Builder;

class QueryBuilder implements QueryBuilderInterface
{
    private $builder;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    private $sql = '';

    
    
    public function select(string $tableName, array $fields = []): QueryBuilderInterface
    {
        $fieldCollection = $this->builder->fields()->set($fields);

        $this->sql .= "SELECT " . $fieldCollection->getSQL() . " FROM `" . $tableName . "` ";

        return $this;
    }

    
    public function addWhere(string $field, string $condition,  $value): QueryBuilderInterface
    {
        $this->builder->where()->add($field,$condition, $value);

        $this->sql .= $this->builder->where()->getSQL();

        return $this;
    }

  
    public function getSQL(): string
    {
        return trim($this->sql);
    }
}
