<?php

namespace App\DB\Builder;

class WhereCollection implements CollectionInterface
{
    private $whereClause = [];

    public function add(string $field, $condition, $value): WhereCollection
    {
        $this->whereClause[] = [
            'field' => $field,
            'condition' => $condition,
            'value' => $value,
        ];

        return $this;
    }

    public function set(array $where = []): CollectionInterface
    {
        $this->whereClause = $where;
        return $this;
    }

    public function getSQL(): string
    {
        $sql = '';
        $counter = 0;
        $count = count($this->whereClause);

        if ($count > 0) {
            $sql .= 'WHERE ';
        }
        foreach ($this->whereClause as $where) {
            $counter++;
            $sql .= '`' . $where['field'] . '` ' . $where['condition'] . ' \'' . $where['value'] . '\'';

            if ($counter < $count) {
                $sql .= ' AND ';
            }

        }
        return $sql;

    }
}
