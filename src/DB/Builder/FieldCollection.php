<?php

namespace App\DB\Builder;

class FieldCollection implements CollectionInterface
{
    private array $fields = [];

    public function add(string $name): FieldCollection
    {
        $this->fields[] = $name;

        return $this;
    }

    public function set(array $fields = []): CollectionInterface
    {
        $this->fields = [];
        foreach ($fields as $field) {
            $this->add($field);
        }
        return $this;
    }

    public function getSQL(): string
    {
        $sql = '*';
        if(count($this->fields) > 0) {
            $sql = '';
            for ($i = 0; $i < count($this->fields); $i++) {
                if ($i > 0) {
                    $sql .= ", ";
                }
                $sql .= "`" . $this->fields[$i] . "`";
            }
        }
        return $sql;
    }
}