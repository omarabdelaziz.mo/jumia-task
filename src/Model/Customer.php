<?php

namespace App\Model;

use Config\Config;

class Customer
{

    protected const  COUNTRY_CODE_PATTERN = '{\(([^\)]+)\)}';

    private $id = '';

    private $name = '';

    private $phone = '';

    private $phoneCountryCode = '';

    public function getPhoneCountryCode()
    {
        return preg_match(self::COUNTRY_CODE_PATTERN, $this->phone, $this->phoneCountryCode) ? $this->phoneCountryCode[1] : '';
    }

    public function getNumber()
    {
        return explode(' ', $this->phone)[1];
    }

    public function isPhoneValid()
    {
        return (preg_match(Config::COUNTRY_CODE_REGEX['+'.$this->getPhoneCountryCode()], $this->phone)) ? 'OK' : 'NOK';
        
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function getPhone()
    {
        return $this->phone;
    }

    public function getCountry()
    {
        return Config::COUNTRIES['+'.$this->getPhoneCountryCode()];
    }
}
