<?php

namespace App\Repository;

use App\Model\Customer;

class CustomerRepository extends AbstractRepository
{

    public function findAllBy(string $tableName, array $where = [])
    {
        $this->queryBuilder->select($tableName, ['id', 'name', 'phone']);
        if(!empty($where)) {
            $this->queryBuilder->addWhere($where[0], $where[1], $where[2]);
        }

        $sql = $this->queryBuilder->getSQL();
        $dbCon = $this->connection->open();
        $statement = $dbCon->prepare($sql);
        $statement->execute();
        return $this->customerMapper($statement->fetchAll());
    }

    private function customerMapper($rows)
    {
        $customers = [];
        foreach ($rows as $row) {
            $customer = new Customer();
            $customer->setId($row['id'])->setName($row['name'])->setPhone($row['phone']);
            $customers[] = $customer;
        }

        return $customers;
    }
}