<?php

namespace App\Repository;

use App\DB\Connection;
use App\DB\QueryBuilder;

abstract class AbstractRepository
{
   
    protected $connection;

 
    protected $queryBuilder;

   
    public function __construct(Connection $connection, QueryBuilder $queryBuilder)
    {
        $this->connection = $connection;
        $this->queryBuilder = $queryBuilder;
    }


    abstract public function findAllBy(string $tableName, array $where = []);
}