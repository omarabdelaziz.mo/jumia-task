<?php

namespace App\Route;


class Router
{

    private array $handlers;
    private $notFoundHandler;
    private const METHOD_GET = 'GET';

    public function get(string $path, $handler)
    {
        $this->addHandler(self::METHOD_GET, $path, $handler);
    }

    private function addHandler(string $method, string $path, $handler)
    {
  
        $this->handlers[$method . $path] = [
            'path' => $path,
            'method' => $method,
            'handler' => $handler
        ];

    }

    public function run()
    {
        $requestUri = parse_url($_SERVER['REQUEST_URI']);
        $requestPath = $requestUri['path'];
        $callback = null;
        foreach($this->handlers as $handler) {
            if($handler['path'] === $requestPath) {
                if($handler['method'] === $_SERVER['REQUEST_METHOD']) {
                    $callback = $handler['handler'];
                    $callback();
                }
            }
        }

        if(!$callback) {
            if(!empty($this->notFoundHandler)) {
                call_user_func($this->notFoundHandler);
            }
        }

    }

    public function addNotFoundHandler($handler)
    {
        $this->notFoundHandler = $handler;
    }
}