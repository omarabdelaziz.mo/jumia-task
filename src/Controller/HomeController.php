<?php

namespace App\Controller;

use App\DB\Builder\Builder;
use App\DB\Connection;
use App\DB\QueryBuilder;
use App\Repository\AbstractRepository;
use App\Repository\CustomerRepository;
use Config\Config;

class HomeController
{
    public function execute()
    {

        $connection = new Connection();
        $builder = new Builder();
        $queryBuilder = new QueryBuilder($builder);
        $customerRepository = new CustomerRepository($connection, $queryBuilder);

        $customers = $this->retrieveCustomers($customerRepository);
        if(isset($_GET['validPhone'])) {
            $customers = $this->filterValidCustomer($customers, $_GET['validPhone']);
        }
        return self::render('CustomerList.php', ['customers' => $customers, 'countries' => Config::COUNTRIES]);

    }

    private function retrieveCustomers(AbstractRepository $customerRepository)
    {
        if (isset($_GET['country'])) {
            return $customerRepository->findAllBy('customer', ['phone', 'like', '%' . Config::COUNTRY_CODE_CODE[$_GET['country']] . '%']);
        }
        return $customerRepository->findAllBy('customer');
    }

    private function filterValidCustomer(array $customers,string $valid)
    {
        $customersWithValidPhone  = []; // array of Customer
        foreach ($customers as $customer) {
            if ($customer->isPhoneValid() === $valid) {
                $customersWithValidPhone[] = $customer;
            }
        }

        return $customersWithValidPhone;
    }


    public static function render(string $template, array $parameters = [])
    {
        ob_start();
        extract($parameters, EXTR_SKIP);
        include $_SERVER['DOCUMENT_ROOT'] . '/views/' . $template;
    }
}
